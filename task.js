const http = require('http')
const server = http.createServer((request, response) => {
    if (request.url === '/speed')
    {
        setTimeout(()=>{
        const data = 'Processing completed'
        response.write(data)
        console.log(performance.now().toFixed(2)) // отсчёт времени от начала работы скрипта 
        response.end()                            // (в миллисекундах, два знака после запятой)
        }, 4000)
    }
}).listen(4444)


